<?php

namespace App\Controller;

use App\Document\CacheLog;
use App\Entity\Cache;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use OldSound\RabbitMqBundle\RabbitMq\Producer;
use Predis\ClientInterface;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CacheController extends AbstractFOSRestController
{
    /** @var Producer */
    private $producer;

    /** @var ClientInterface */
    private $redis;

    /**
     * CacheController constructor.
     * @param Producer $producer
     * @param ClientInterface $redis
     */
    public function __construct(Producer $producer, ClientInterface $redis)
    {
        $this->producer = $producer;
        $this->redis = $redis;
    }

    /**
     * Список тайников
     *
     * Возвращает список тайников
     *
     * @SWG\Response(
     *     response="200",
     *     description="Список тайников",
     *     @SWG\Schema(
     *          type="object",
     *          title="list",
     *          @SWG\Property(
     *              property="caches",
     *              type="array",
     *              items=@SWG\Items(ref=@Model(type=Cache::class, groups={"list"}))
     *          )
     *     )
     * )
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     type="integer",
     *     description="Позиция первого элемента выдачи"
     * )
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     type="integer",
     *     description="Ограничение количества тайников в выдаче"
     * )
     * @SWG\Tag(name="Тайники")
     * @Rest\Get("/caches.json")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function listAction(Request $request, EntityManagerInterface $entityManager)
    {
        $offset = $request->get('offset', 0);
        $limit = $request->get('limit', 25);
        $caches = $entityManager
            ->getRepository(Cache::class)
            ->findBy([], ['id' => 'ASC'], $limit, $offset)
        ;

        return $this->handleView(
            $this
                ->view([
                    'caches' => $caches,
//                    'count' =>    todo: общее количество
                ])
                ->setContext(
                    (new Context())->setGroups(['list'])
                )
        );
    }

    /**
     * Детальная информация по тайнику
     *
     * Возвращает детальную информацию по тайнику
     *
     * @SWG\Response(
     *     response="200",
     *     description="Детальная информация по тайнику",
     *     @Model(type=Cache::class, groups={"Default", "details"})
     * )
     * @SWG\Response(
     *     response="404",
     *     description="Тайник не найден"
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     description="Идентификатор тайника"
     * )
     * @SWG\Tag(name="Тайники")
     * @Rest\Get("/caches/{id}.json", name="app_cache_get")
     * @param EntityManagerInterface $entityManager
     * @param int $id
     * @return Response
     */
    public function getAction(EntityManagerInterface $entityManager, int $id)
    {
        $key = 'cache_' . $id . '.json';
        if ($this->redis->exists($key)) {
            $content = $this->redis->get($key);
            return new Response($content);
        }
        $cache = $entityManager
            ->getRepository(Cache::class)
            ->find($id)
        ;
        if (!$cache) {
            throw new NotFoundHttpException();
        }
        $response = $this->handleView($this->view($cache));
        $content = $response->getContent();
        $this->redis->set($key, $content);
        return $response;
    }

    /**
     * Создание тайника
     *
     * Создаёт новый тайник
     *
     * @SWG\Response(
     *     response="201",
     *     description="Тайник создан",
     *     @Model(type=Cache::class, groups={"Default", "details"})
     * )
     * @SWG\Response(
     *     response="202",
     *     description="Задание на создание тайника поставлено в очередь"
     * )
     * @SWG\Response(
     *     response="422",
     *     description="Ошибка валидации"
     * )
     * @SWG\Parameter(
     *     name="cache",
     *     in="body",
     *     description="Данные тайника",
     *     required=true,
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="name", type="string", example="Тайник 42"),
     *          @SWG\Property(property="description", type="string", example="Крутой тайник с ништяками!"),
     *          @SWG\Property(property="lat", type="number", example=45.5),
     *          @SWG\Property(property="lon", type="number", example=45.5),
     *          required={"name", "description", "lat", "lon"}
     *     )
     * )
     * @SWG\Parameter(
     *     name="async",
     *     in="query",
     *     type="boolean",
     *     default="false",
     *     description="Создание через очередь"
     * )
     * @SWG\Tag(name="Тайники")
     * @Rest\Post("/caches.json")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function createAction(
        Request $request,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        ValidatorInterface $validator
    )
    {
        if ($request->get('async', false)) {
            //Это искуственный пример, поэтому так допустимо.
            //На практике лучше сразу положить все данные в сущность,
            // а воркеру отдать только непосредственно долгую операцию.
            $data = [
                'name' => $request->request->get('name'),
                'description' => $request->request->get('description'),
                'lat' => $request->request->get('lat'),
                'lon' => $request->request->get('lon'),
            ];
            $this->producer->publish(json_encode($data));
            $cacheLog = new CacheLog();
            $cacheLog->setAction('async_create');
            $documentManager->persist($cacheLog);
            $documentManager->flush();
            return new Response(null, Response::HTTP_ACCEPTED);
        }
        $cache = new Cache();
        $cache
            ->setName($request->request->get('name'))
            ->setDescription($request->request->get('description'))
            ->setLat($request->request->get('lat'))
            ->setLon($request->request->get('lon'))
        ;
        $errors = $validator->validate($cache);
        if (count($errors) > 0) {
            return $this->handleView(
                $this->view($errors)
                    ->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            );
        }
        $entityManager->persist($cache);
        $entityManager->flush();
        $cacheLog = new CacheLog();
        $cacheLog
            ->setAction('create')
            ->setCacheId($cache->getId())
        ;
        $documentManager->persist($cacheLog);
        $documentManager->flush();
        return $this->handleView(
            $this->view($cache)
                ->setStatusCode(Response::HTTP_CREATED)
                ->setLocation($this->generateUrl(
                    'app_cache_get',
                    ['id' => $cache->getId()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ))
        );
    }

    /**
     * Изменение тайника
     *
     * Обновляет данные тайника. Принимает список полей и их новые значения.
     * Значения остальных полей останутся без изменения.
     *
     * @SWG\Response(
     *     response="200",
     *     description="Тайник обновлён",
     *     @Model(type=Cache::class, groups={"Default", "details"})
     * )
     * @SWG\Response(
     *     response="202",
     *     description="Задание  на обновление тайника поставлено в очередь"
     * )
     * @SWG\Response(
     *     response="404",
     *     description="Тайник не найден"
     * )
     * @SWG\Response(
     *     response="422",
     *     description="Ошибка валидации"
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     description="Идентификатор тайника"
     * )
     * @SWG\Parameter(
     *     name="cache",
     *     in="body",
     *     description="Данные тайника",
     *     required=true,
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="name", type="string", example="Тайник 42"),
     *          @SWG\Property(property="description", type="string", example="Крутой тайник с ништяками!"),
     *          @SWG\Property(property="lat", type="number", example=45.5),
     *          @SWG\Property(property="lon", type="number", example=45.5)
     *     )
     * )
     * @SWG\Parameter(
     *     name="async",
     *     in="query",
     *     type="boolean",
     *     default="false",
     *     description="Обновление через очередь"
     * )
     * @SWG\Tag(name="Тайники")
     * @Rest\Patch("/caches/{id}.json")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param ValidatorInterface $validator
     * @param int $id
     * @return Response
     */
    public function updateAction(
        Request $request,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        ValidatorInterface $validator,
        int $id
    )
    {
        if ($request->get('async', false)) {
            $data = ['id' => $id];
            if ($request->request->has('name')) {
                $data['name'] = $request->request->get('name');
            }
            if ($request->request->has('description')) {
                $data['description'] = $request->request->get('description');
            }
            if ($request->request->has('lat')) {
                $data['lat'] = $request->request->get('lat');
            }
            if ($request->request->has('lon')) {
                $data['lon'] = $request->request->get('lon');
            }
            $this->producer->publish(json_encode($data));
            $cacheLog = new CacheLog();
            $cacheLog
                ->setAction('async_update')
                ->setCacheId($id)
            ;
            $documentManager->persist($cacheLog);
            $documentManager->flush();
            return new Response(null, Response::HTTP_ACCEPTED);
        }
        $cache = $entityManager
            ->getRepository(Cache::class)
            ->find($id)
        ;
        if (!$cache) {
            throw new NotFoundHttpException();
        }
        if ($request->request->has('name')) {
            $cache->setName($request->request->get('name'));
        }
        if ($request->request->has('description')) {
            $cache->setDescription($request->request->get('description'));
        }
        if ($request->request->has('lat')) {
            $cache->setLat($request->request->get('lat'));
        }
        if ($request->request->has('lon')) {
            $cache->setLon($request->request->get('lon'));
        }
        $errors = $validator->validate($cache);
        if (count($errors) > 0) {
            return $this->handleView(
                $this->view($errors)
                    ->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            );
        }
        $entityManager->flush();
        $cacheLog = new CacheLog();
        $cacheLog
            ->setAction('update')
            ->setCacheId($cache->getId())
        ;
        //если работаем с несколькими форматами, то надо удалить все
        $keys = ['cache_' . $id . '.json'];
        $this->redis->del($keys);
        $documentManager->persist($cacheLog);
        $documentManager->flush();
        return $this->handleView($this->view($cache));
    }

    /**
     * Удаление тайника
     *
     * Удаляет тайник
     *
     * @SWG\Response(
     *     response="204",
     *     description="Тайник удалён"
     * )
     * @SWG\Response(
     *     response="404",
     *     description="Тайник не найден"
     * )
     * @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     type="integer",
     *     description="Идентификатор тайника"
     * )
     * @SWG\Tag(name="Тайники")
     * @Rest\Delete("/caches/{id}.json")
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param int $id
     * @return Response
     */
    public function deleteAction(EntityManagerInterface $entityManager, DocumentManager $documentManager, int $id)
    {
        $cache = $entityManager
            ->getRepository(Cache::class)
            ->find($id)
        ;
        if (!$cache) {
            throw new NotFoundHttpException();
        }
        $cacheLog = new CacheLog();
        $cacheLog
            ->setAction('remove')
            ->setCacheId($cache->getId())
        ;
        $keys = ['cache_' . $id . '.json'];
        $this->redis->del($keys);
        $entityManager->remove($cache);
        $entityManager->flush();
        $documentManager->persist($cacheLog);
        $documentManager->flush();
        return new Response(null, Response::HTTP_NO_CONTENT);
    }
}
