<?php

namespace App\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;

/**
 * @Document(collection="cache_log")
 */
class CacheLog
{
    /**
     * @Id()
     */
    private $id;

    /**
     * @var string
     * @Field
     */
    private $action;

    /**
     * @var int
     * @Field(name="cache_id")
     */
    private $cacheId;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return CacheLog
     */
    public function setAction(string $action): self
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return int
     */
    public function getCacheId(): int
    {
        return $this->cacheId;
    }

    /**
     * @param int $cacheId
     * @return CacheLog
     */
    public function setCacheId(int $cacheId): self
    {
        $this->cacheId = $cacheId;
        return $this;
    }
}
