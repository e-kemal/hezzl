<?php

namespace App\Consumer;

use App\Entity\Cache;
use Doctrine\ORM\EntityManagerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Predis\ClientInterface;

class CacheConsumer implements ConsumerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ClientInterface
     */
    private $redis;

    /**
     * CacheConsumer constructor.
     * @param EntityManagerInterface $entityManager
     * @param ClientInterface $redis
     */
    public function __construct(EntityManagerInterface $entityManager, ClientInterface $redis)
    {
        $this->entityManager = $entityManager;
        $this->redis = $redis;
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $msg)
    {
        $data = json_decode($msg->getBody(), true);
        if (array_key_exists('id', $data)) {
            $cache = $this
                ->entityManager
                ->getRepository(Cache::class)
                ->find($data['id'])
            ;
            $keys = ['cache_' . $cache->getId() . '.json'];
            $this->redis->del($keys);
        }
        else {
            $cache = new Cache();
            $this->entityManager->persist($cache);
        }
        if (array_key_exists('name', $data)) {
            $cache->setName($data['name']);
        }
        if (array_key_exists('description', $data)) {
            $cache->setDescription($data['description']);
        }
        if (array_key_exists('lat', $data)) {
            $cache->setLat($data['lat']);
        }
        if (array_key_exists('lon', $data)) {
            $cache->setLon($data['lon']);
        }
        $this->entityManager->flush();
    }
}
