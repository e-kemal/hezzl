<?php

namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Swagger\Annotations as SWG;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Cache
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"Default", "list"})
     * @SWG\Property(description="Уникальный идентификатор тайника")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @Serializer\Groups({"Default", "list"})
     * @SWG\Property(description="Название тайника")
     * @Assert\NotBlank(message="Name should not be blank")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @Serializer\Groups({"details"})
     * @SWG\Property(description="Описание тайника")
     * @Assert\NotBlank(message="Description should not be blank")
     */
    private $description;

    /**
     * @var float
     * @ORM\Column(type="float")
     * @Serializer\Groups({"Default", "list"})
     * @SWG\Property(description="Долгота расположения тайника")
     * @Assert\NotBlank()
     * @Assert\LessThanOrEqual(180, message="Longitude should be between -180 and 180 degrees")
     * @Assert\GreaterThanOrEqual(-180, message="Longitude should be between -180 and 180 degrees")
     */
    private $lon;

    /**
     * @var float
     * @ORM\Column(type="float")
     * @Serializer\Groups({"Default", "list"})
     * @SWG\Property(description="Широта расположения тайника")
     * @Assert\NotBlank()
     * @Assert\LessThanOrEqual(90, message="Latitude should be between -90 and 90 degrees")
     * @Assert\GreaterThanOrEqual(-90, message="Latitude should be between -90 and 90 degrees")
     */
    private $lat;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @return Cache
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $description
     * @return Cache
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param float $lon
     * @return Cache
     */
    public function setLon(?float $lon): self
    {
        $this->lon = $lon;
        return $this;
    }

    /**
     * @return float
     */
    public function getLon(): float
    {
        return $this->lon;
    }

    /**
     * @param float $lat
     * @return Cache
     */
    public function setLat(?float $lat): self
    {
        $this->lat = $lat;
        return $this;
    }

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }
}
